/*This is an Example of React Native Map*/
import React from 'react';
import { StyleSheet, Text, View ,  ScrollView, Alert, TouchableOpacity} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Modal from 'react-native-modal';
import Moment from 'moment';
import * as country from './country.js';

export default class MapChart extends React.Component {


  constructor(props) {
    super(props); 
      this.state = {
        isModalVisible: false,
        isTableModalVisible: false,
        displayData: {},
        dataSource: [],
        loading: true,
        newMarkers: [],
        HeadTable: [],
        DataTable: []
      }
  }

  componentDidMount() {
    // this.createMarker(country.DATA);
    fetch("https://thevirustracker.com/free-api?countryTotals=ALL")
    .then(response => response.json())
    .then((responseJson)=> {
      this.mapResponse(responseJson.countryitems);
      this.setState({ loading: false, dataSource: responseJson.countryitems}, function() {
      });
    })
    .catch(error => console.log(error))
  }

  createMarker(data) {
    let markers = [];
    let marker = {};

    for (let i = 0; i < data.length; i++){
      if (data[i].total_active_cases){
        marker = {
          id: i,
          title: data[i].title,
          subtitle: this.totalCount(data[i]),
          description: data[i],
          coordinates: {
            latitude: parseFloat(data[i].prop2),
            longitude: parseFloat(data[i].prop3)
          }
        }
        markers.push(marker);
      }
    }

    this.setState({
      newMarkers: markers
    });

    this.setState({ newMarkers: markers}, function() {
    });
  }

  totalCount(data) {
    if (data.total_active_cases){
      return `Total active cases: ${data.total_active_cases.toString()}`
    }
  }

  totalRecovered(data) {
    if (data.total_active_cases){
      return `Total recovered cases: ${data.total_recovered.toString()}`
    }
  }

  openModal = (data) =>{
    this.setState({
      isModalVisible: true,
      displayData: data
    })
  }

  closeModal = () =>{
    this.setState({
      isModalVisible: false,
      displayData: {}
    })
  }

  toggleModal = () =>{
    this.setState({
      isModalVisible: !this.state.isModalVisible
    })
  }

  openTableModal = (head, data) =>{
    this.setState({
      isTableModalVisible: true,
      HeadTable: head,
      DataTable: data
    })
  }

  closeTableModal = () =>{
    this.setState({
      isTableModalVisible: false,
      HeadTable: [],
      DataTable: []
    })
  }

  toggleTableModal = () =>{
    this.setState({
      isTableModalVisible: !this.state.isTableModalVisible
    })
  }


  mapResponse(data) {
   let newData = [];
   let obj = {};

    // console.log(data[0])
    function countProperties (obj) {
        var count = 0;
    
        for (var property in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, property)) {
                count++;
            }
        }
    
        return count;
    }
    var Count = countProperties(data[0]);
  

    for (let i = 1; i < Count-1; i++){
      obj = data[0][i.toString()];
      newData.push(obj)
    }

    let arr1 = country.DATA

    let merged = [];
    for(let i= 0; i < arr1.length; i++) {
      merged.push({
      ...arr1[i], 
      ...(newData.find((itmInner) => itmInner.code === arr1[i].code))}
      );
    }
    
 
    merged.filter(function( obj ) {
      if (obj.total_active_cases == undefined){
        merged.splice(merged.indexOf(obj), 1);
      }
    });

    // console.log(merged);
    this.createMarker(merged);
  }


  fetchDetailData(code, country) {
    fetch(`https://api.thevirustracker.com/free-api?countryTimeline=${code}`)
    .then(response => response.json())
    .then((responseJson) => {
      this.generateTableData(responseJson.timelineitems[0], country);
    })
    .catch(error => console.log(error))  
  }

  generateTableData(data, country) {
    var dates = [];
    var tableData = [];
    let tabObj = {};


    dates = Object.keys(data);
    for (let i = 0; i < dates.length - 1; i++){
      tabObj = data[dates[i]];
      tabObj.date = dates[i]
      tableData.push(tabObj);
    }

    var latest = [];
    latest = tableData.slice(Math.max(tableData.length - 4, 1))

    var lDate = [];
    var lDeaths = [];
    var lNewCases = [];
    var total = [];
    var object = {};

    lDate = latest.map(e => Moment(e.date).format('MMM D'));
    lDeaths = latest.map(e => e.new_daily_deaths);
    lNewCases = latest.map(e => e.new_daily_cases);
    total = latest.slice(Math.max(latest.length - 1, 1))

    object = {
      dates: lDate,
      deaths: lDeaths,
      new_cases: lNewCases,
      total_cases: total[0]
    };

    this.props.navigation.navigate('Details', { data: object, country: country });
  }


  generateArrays(keys, tableData) {
    var output = tableData.map(function(obj) {
      return Object.keys(obj).sort().map(function(key) { 
        return obj[key];
      });
    });
    this.props.navigation.navigate('Details', { data: { HeadTable: keys, DataTable: output } });
  }

  redirectToDetailPage(code, country) {
    fetch(`https://api.thevirustracker.com/free-api?countryTimeline=${code}`)
    .then(response => response.json())
    .then((responseJson) => {
      this.closeModal();
      this.generateTableData(responseJson.timelineitems[0], country);
    })
    .catch(error => console.log(error))
    // this.fetchDetailData(code);
  }

  onRegionChange(region) {
    this.setState({ region });
  }
  render() {
    var mapStyle=[{"elementType": "geometry", "stylers": [{"color": "#242f3e"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#746855"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#242f3e"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#263c3f"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#6b9a76"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#38414e"}]},{"featureType": "road","elementType": "geometry.stroke","stylers": [{"color": "#212a37"}]},{"featureType": "road","elementType": "labels.text.fill","stylers": [{"color": "#9ca5b3"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#746855"}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#1f2835"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#f3d19c"}]},{"featureType": "transit","elementType": "geometry","stylers": [{"color": "#2f3948"}]},{"featureType": "transit.station","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#17263c"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#515c6d"}]},{"featureType": "water","elementType": "labels.text.stroke","stylers": [{"color": "#17263c"}]}];


    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          showsUserLocation={true}
          followUserLocation={true}
          zoomEnabled={true}
          customMapStyle={mapStyle}
        >
          {this.state.newMarkers.map(marker => (
            <MapView.Marker 
              coordinate={marker.coordinates}
              title={marker.title}
              description={marker.subtitle}
              key={marker.id}
              // onCalloutPress={() => alert(this.totalRecovered(marker.description))}
              onCalloutPress={()=>this.openModal(marker.description)}
            />
          ))}
        </MapView>

        {/* First Modal */}
        <Modal animationIn="slideInUp" animationOut="slideOutDown" onBackdropPress={()=>this.closeModal()} onSwipeComplete={()=>this.closeModal()} swipeDirection="right" isVisible={this.state.isModalVisible}>
          <View style={styles.modal}>
           
            <View style={styles.heading}>
              <Text style={styles.title}> Information </Text>
            </View>
            
            <View style={styles.body}>

              <View style={styles.parentRowOne}>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}> Total Count: </Text>
                </View>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}>{this.state.displayData.total_cases}</Text>
                </View>
              </View>

              <View style={styles.parentRow}>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}> Active cases: </Text>
                </View>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}>{this.state.displayData.total_active_cases}</Text>
                </View>
              </View>

              <View style={styles.parentRow}>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}> Critical cases: </Text>
                </View>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}>{this.state.displayData.total_serious_cases}</Text>
                </View>
              </View>

              <View style={styles.parentRow}>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}> Recovered cases: </Text>
                </View>
                <View style={styles.row}>
                  <Text style={{textAlign:'center'}}>{this.state.displayData.total_recovered}</Text>
                </View>
              </View>

              <View style={{ flex: 1,justifyContent:'center', bottom: 0, borderBottomEndRadius: 10, borderBottomStartRadius: 10}}>
                <View style={{flexDirection:'row'}}>
                  <TouchableOpacity style={{width:'50%'}} onPress={() => this.redirectToDetailPage(this.state.displayData.code, this.state.displayData.title)}>
                    <Text style={{color:'skyblue',textAlign:'center',padding:10}}>Know more</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'50%'}} onPress={()=>this.closeModal()}>
                    <Text style={{color:'skyblue',textAlign:'center',padding:10}}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            
            </View>
          </View>
        </Modal>
      
      </View>
    );
  }
} 
 
const styles = StyleSheet.create({
  container: {
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  map: {
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
  },
  body: {
    backgroundColor: 'white',
    height: 230,
    justifyContent:'center',
    textAlign:'center',
    borderBottomStartRadius: 10,
    borderBottomEndRadius: 10
  },
  row: {
    fontFamily: 'Proxima Nova'
  },
  parentRow: {
    flex: 1, 
    flexDirection: 'row',
    marginLeft: 80,
  },
  parentRowOne: {
    flex: 1, 
    flexDirection: 'row',
    marginLeft: 80,
    marginTop: 10
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Proxima Nova',
    color: 'white'
  },
  heading: {
    backgroundColor: 'skyblue',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  tablebody: {
    height: 200
  }
});

// path for release apk = app/android/app/build/outputs/release