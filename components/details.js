import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Button,
  Text,
  Dimensions
} from 'react-native';

import {
  Colors
} from 'react-native/Libraries/NewAppScreen';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";

import 'react-native-gesture-handler';


const Details: () => React$Node = ({ route, navigation }) => {

   const data  = route.params.data;
    
    return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>          
                <View style={styles.chart}>
                    <Text style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                    }}>Daily Deaths in {route.params.country}</Text>
                    <LineChart
                        data={{
                            labels: data.dates,
                            datasets: [
                                {
                                    data: data.deaths
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width - 40} // from react-native
                        height={250}
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                            }
                        }}
                        bezier style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>

                <View style={styles.chart}>
                    <Text style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                    }}>Daily New Cases in {route.params.country}</Text>
                    <BarChart
                        data={{
                            labels: data.dates,
                            datasets: [
                            {
                                data: data.new_cases
                            }
                            ]
                        }}
                        width={Dimensions.get("window").width - 40} // from react-native
                        height={250}
                        chartConfig={{
                          backgroundColor: "skyblue",
                          backgroundGradientFrom: "skyblue",
                          backgroundGradientTo: "skyblue",
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#ffa726"
                        }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                        verticalLabelRotation={30}
                    />
                </View>
                <View>
                    <Text style={styles.lastLabel}>Total deaths in {route.params.country}: {data.total_cases.total_deaths}</Text>
                </View>
                <View>
                    <Text style={styles.lastLabel}>Total recovered in {route.params.country}: {data.total_cases.total_recoveries}</Text>
                </View>
                <View>
                    <Text style={styles.lastLabel}>Total Cases in {route.params.country}: {data.total_cases.total_cases}</Text>
                </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
 chart: {
     width: Dimensions.get("window").width - 40
 },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
    height: Dimensions.get("window").height + 210
  },
  sectionContainer: {
    marginTop: 0,
    paddingHorizontal: 24,
    height: 580,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 0,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  lastLabel: {
    fontSize: 18,
    padding: 10,
  }
});

export default Details;
