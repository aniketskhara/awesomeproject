import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  StyleSheet, Dimensions
} from 'react-native'
import Main from './components/main';
import Details from './components/details';

const Stack = createStackNavigator();


function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Main} headerStyle={styles.headerStyle} options={{ title: 'World Wide Corona Update',           
        headerStyle: {
            backgroundColor: '#f4511e',
            height: 30
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontSize: 15,
          fontWeight: 'bold',
          textAlign: 'justify',
          marginLeft:  Dimensions.get("window").width - 285
        } 
        }}/>
        <Stack.Screen name="Details" component={Details} headerStyle={styles.headerStyle}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({ 
  headerStyle: {
    color: 'red',
    backgroundColor: 'green'
  }
});
export default App;